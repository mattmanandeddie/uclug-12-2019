# Christmas Lights w/some LINUX

* Plan, Plan, Plan

* Start small and build bigger

* Measure Roof line and take a photo of your scene

* Pixel Density
    * 30-144 Leds per meter or 3 feet 3 inch

* 12V vs. 5V
    * 12V less color loss
    * 12V more $ & more power
    * 5V inject power more often
    * 5V less $ & less power
* Power Supply
    * typical 5v RGB led 20mA-60mA
    * 100 led * 60mA / 1000 = 6A
    * Wall warts 5A
    * Laptop bricks 5A-15A
    * These Type 5A-60+A
    ![](https://i.ebayimg.com/images/g/CQMAAOSwZ8ZW8px7/s-l300.jpg)
    * Make your own out of an old ATX power Supply with one of theses
    ![](https://cdn.shopify.com/s/files/1/2386/9605/products/ATX-Benchtop-Power-Supply-Board-pmdway-1_622x317.jpg?v=1529727139)
        
        ----
        
* Types of lights
    *  WS2811 
        *  Good for Stings
    *  WS2812B
        *  Most Common
    *  WS2812Eco
    *  WS2813
        *  Duplicate Data Line
    *  WS2815
        *  Separate White Led
    *  SK6812
    *  SK9822
    *  APA102
        *  SPI Clock and Data 
    * [Watch this video for good info on types of lights](https://www.youtube.com/watch?v=QnvircC22hU)
    * Strip Lights
    ![](https://images-na.ssl-images-amazon.com/images/I/61xtxIK65eL._SX466_.jpg)

    * Strips come with Black and White PCB
    ![](http://www.ledlights-uk.com/images/1/en32986012421.jpg)
    
    * String Lights
    ![](https://images-na.ssl-images-amazon.com/images/I/71kuC8MiyZL._SX425_.jpg)

* [IPS Rating Explained](https://www.enclosurecompany.com/ip-ratings-explained.php)
    * IPS 20/30/40 Indoor
    * IPS 65 Water Resistant
    * IPS 67 Waterproof

* Installation
    * Aluminum Profile Channels
    ![](https://www.derrywood.com/wp-content/uploads/2017/08/led-profile-17.jpg)
    * [Good video on how PVC and vinyl look](https://www.youtube.com/watch?v=tXvtxwK3jRk)
    * Vinyl siding J channel
    ![](https://mobileimages.lowes.com/product/converted/842675/842675123956.jpg?size=xl)
    
    * Thin Wall PVC
    ![](https://hw.menardc.com/main/items/media/CHARL008/ProductLarge/PR160BE.jpg)
        
* Controllers
    * Arduino
    ![](https://hackster.imgix.net/uploads/attachments/322048/f38hynlj4ofs290_large_z72wAYHngx.jpg?auto=compress%2Cformat&w=900&h=675&fit=min)
        
    * ESP8266(Wi-Fi) or ESP32(Bluetooth & Wi-Fi)
    ![](https://imgaz1.staticbg.com/thumb/large/oaupload/banggood/images/72/71/e1fe609e-851c-42b3-a7f7-256ffda57c9f.jpeg)
        * Logic Level Converter
        ![](https://cdn.sparkfun.com/r/500-500/assets/parts/8/5/2/2/12009-06.jpg)
    
    * PIXEL Controller
    ![](https://www.pixelcontroller.com/store/modules/homeslider/images/0a5b0f4ad5b0970b9bf5885b6c5f65565455b474_F16v3-Slider.png)
    
    * Music controller
    ![](https://images-na.ssl-images-amazon.com/images/I/41kAMAZ8qZL.jpg)
    
    * NEMA Enclosure
        ![](https://i5.walmartimages.com/asr/88f3ecee-99a0-4697-a058-034962e3af0d_1.e932d1df00b0c7ffff576d25d56e6a95.jpeg?odnWidth=450&odnHeight=450&odnBg=ffffff)
    * Software and Firmware
        * [WLED](https://github.com/Aircoookie/WLED) - Firmware for ESP
            * [How to flash ESPxx|xx with esptool](https://nodemcu.readthedocs.io/en/master/flash/)
            * [GitHub esptool](https://github.com/espressif/esptool)
            * [MQTT compatible](https://en.wikipedia.org/wiki/MQTT)
            * [E.131/DMX512 compatible](https://www.doityourselfchristmas.com/wiki/index.php?title=E1.31_(Streaming-ACN)_Protocol)
        * [xLights](https://xlights.org/) - The *"Photoshop"* of Music light effects software
            * [277 page user manual](https://xlights.org/xLights_User_Manual.pdf)
        * [LedFx](https://github.com/ahodges9/LedFx) - Python music light controller software. / My laptop audio input had issues
        * [Audio Reactive LED Strip](https://github.com/scottlawsonbc/audio-reactive-led-strip) - Music lights controller software. / Did not test
        * [ESPixelStick](https://github.com/forkineye/ESPixelStick) - ESP firmware for arduino IDE. / Did not test
        * Falcon Pi Player. / Load on Pi via NOOBS......
        
        
        
        
        
        
        
        
        
        
        
        
        
        

